// Put any other imports below so that CSS from your
// components takes precedence over default styles.

import React from "react";

import App from "./js/components/app";
import About from './js/components/about';
import Register from './js/components/register';
import ReactDOM from 'react-dom';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import Navigation from './js/components/navigation';
import Login from './js/components/login';
import Farmgrid from './js/components/farmgrid';
import Edit from './js/components/edit';

ReactDOM.render((
    <wrapper>
        <BrowserRouter>
            <div>
                <Navigation />
                <Switch>
                	<Route path="/about" component={About}/>
					<Route exact path="/register" component={Register}/>
					<Route exact path="/edit" component={Edit}/>
					<Route exact path="/details" component={Farmgrid}/>
					<Route exact path="/login" component={Login}/>
					<Route exact path="/" component={App}/>
                  }

                </Switch>
            </div>
        </BrowserRouter>
    </wrapper>
    ),
    document.getElementById('root')
);

