/**
 * Created by kavitha on 11/07/17.
 */
import React, {Component} from "react";
import ReactDOM from 'react-dom';
import {Link} from "react-router-dom";
import map from '../images/Chitradurga.jpg';

class appHeader extends Component {
    render() {
        return (
				<div className="appHeader">
				    <div id="page-header">
						<div id="map">
							<img src={map} width="300" height="300" />
							<h1 className="textcolor">ಚಿತ್ರದುರ್ಗ ರೈತರ ಆತ್ಮಹತ್ಯೆ ಅರ್ಹ ಪ್ರಕರಣಗಳಿಗೆ ಸವಲತ್ತು ಒದಗಿಸಲು ಇರುವ ಪ್ರಕರಣಗಳ ವಿವರ</h1>
                        <h3 className="textcolor">ಕರ್ನಾಟಕ ರಾಜ್ಯ ಕೃಷಿ ಇಲಾಖೆ </h3>
						</div>	
                        
                    </div>
                    <hr/>
                    <div align="center">
                    <Link to="/about" className="btn-lg btn-info">ಹೆಚ್ಚಿನ ಮಾಹಿತಿಗೆ</Link>
                </div>

                </div>
        );
    }
}
export default (appHeader);
