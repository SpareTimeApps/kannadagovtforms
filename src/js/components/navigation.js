/**
* Created by kavitha on 12/07/17.
*/

// Put any other imports below so that CSS from your
// components takes precedence over default styles.

import React, {Component} from "react";

import {LinkContainer} from 'react-router-bootstrap';
import '../../scss/style.scss';
import logo from '../images/ag-logo.jpg';
import login from '../images/login.png';
import navToggle from '../images/navToggle.jpg';
import userIcon from '../images/UserIcon.svg';

import {
Collapse,
Navbar,
NavbarToggler,
NavbarBrand,
Nav,
NavItem,
NavLink,
UncontrolledDropdown,
DropdownToggle,
DropdownMenu,
DropdownItem } from 'reactstrap';

	class navInstance extends Component {
	constructor(props) {
	super(props);

	this.toggle = this.toggle.bind(this);
	this.state = {
	isOpen: false
	};
	}
	toggle() {
	this.setState({
	isOpen: !this.state.isOpen
	});
	}
	render() {

		return (
			<div>
				<div class=".container-fluid">
						<div class="header-content">
							<div class="site-logo main-header--site-logo">
								<img src={logo} width="100" height="80" />
							</div>
						</div>					
				</div>
		
				<div>
					<Navbar id="secondnav" expand="md">		
						<NavbarBrand id="secondnavlink" href="/">Department name(brand)</NavbarBrand>
						<NavbarToggler onClick={this.toggle}><img src={navToggle} width="20" height="20" /></NavbarToggler>
						<Collapse isOpen={this.state.isOpen} navbar>
						<Nav className="ml-auto" navbar>
							
							<NavItem>
								<NavLink id="secondnavlink" href="/about"></NavLink>
								<NavLink id="secondnavlink" href="/about">ಮುಖಪುಟ  | </NavLink>
								<NavLink id="secondnavlink" href="/register">ರೈತ ವಿವರ ನೋಂದಣಿ  | </NavLink>
								<NavLink id="secondnavlink" href="/edit">ರೈತ ಡೇಟಾವನ್ನು ಬದಲಾಯಿಸಿ  | </NavLink>
								<NavLink id="secondnavlink" href="/details">ರೈತ ವಿವರ  | </NavLink>
								<NavLink  id="secondnavlink" href="/yearlyreport">ವಾರ್ಷಿಕ ವರದಿ  | </NavLink>
								<NavLink id="secondnavlink"  href="/talukreport">ತಾಲ್ಲೂಕುವಾರು ವರದಿ  | </NavLink>
							</NavItem>
							
							<NavItem>
								<NavLink id="secondnavlink"  href="/login">ಲಾಗಿನ್<img src={userIcon} width="20" height="20" /></NavLink>
							</NavItem>
						</Nav>
						</Collapse>
					</Navbar> 
				</div>
			</div>

		);

		}
}

export default (navInstance);
