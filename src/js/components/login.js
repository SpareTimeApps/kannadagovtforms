import React, {Component} from "react";

import {Button, ControlLabel, Form, FormControl, FormGroup} from "react-bootstrap";


const wellStyles = {maxWidth: 400, margin: '0 auto 10px'};

class Contact extends Component {

    render() {
       
        return (
            <div className="center">

                <div class="form-group">
                  
                    <Form method="POST" action="/contactsiteadmin">
                        <FormGroup controlId="formControlsText">
                            <ControlLabel>Name</ControlLabel>
                            <FormControl componentClass="input"  placeholder="field" name="name" />
                        </FormGroup>
                        <FormGroup controlId="formControlsText">
                            <ControlLabel>Email</ControlLabel>
                            <FormControl componentClass="input"  placeholder="field" name="email" />
                        </FormGroup>
                        <FormGroup controlId="formControlsTextarea">
                            <ControlLabel>details</ControlLabel>
                            <FormControl componentClass="textarea"  placeholder="textarea" name="message"/>
                        </FormGroup>
                        <FormGroup controlId="formControlsSubmit">
                            <Button bsStyle="primary" type="submit"
                                     submit-btn>Submit</Button>
                        </FormGroup>
                    </Form>
                </div>
            </div>
        );
    }
}

export default Contact;