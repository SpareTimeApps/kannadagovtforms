/**
 * Created by kavitha on 11/07/17.
 */

import React, {Component} from "react";
import ReactDOM from 'react-dom';

class register extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      fields: {},
      errors: {}
    }
  }

  handleValidation(){
    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    //Name
    if(!fields["name"]){
      formIsValid = false;
      errors["name"] = "ಖಾಲಿ ಬಿಡುವಂತಿಲ್ಲ";
    }

    //Address
    if(!fields["address"]){
      formIsValid = false;
      errors["address"] = "ಖಾಲಿ ಬಿಡುವಂತಿಲ್ಲ";
    }

    //Taluk
    if(!fields["taluk"]){
      formIsValid = false;
      errors["taluk"] = "ಖಾಲಿ ಬಿಡುವಂತಿಲ್ಲ";
    }

	//Compensation
    if(!fields["comp"]){
      formIsValid = false;
      errors["comp"] = "ಖಾಲಿ ಬಿಡುವಂತಿಲ್ಲ";
    }

    this.setState({errors: errors});
    return formIsValid;
  }

  contactSubmit(e){
    e.preventDefault();
    if(this.handleValidation()){
      alert("ಫಾರ್ಮ್ ಸಲ್ಲಿಸಲಾಗಿದೆ");
    }else{
      alert("ಫಾರ್ಮ್ ದೋಷಗಳನ್ನು ಹೊಂದಿದೆ")
    }

  }

  handleChange(field, e){    		
    let fields = this.state.fields;
    fields[field] = e.target.value;        
    this.setState({fields});
  }

  render(){
    return (
	<div class=".container-fluid">
        <form name="contactform" onSubmit= {this.contactSubmit.bind(this)}>
				<br/>
				<legend id="legend">ರೈತ ವಿವರ ನೋಂದಣಿ</legend>
				<div class="form-group">
					<label for="name">ರೈತನ ಹೆಸರು:</label>	
					<input ref="name" type="text" size="30" class="form-control" placeholder="ಹೆಸರು" onChange={this.handleChange.bind(this, "name")} value={this.state.fields["name"]}/><span className="error">{this.state.errors["name"]}</span>
				</div>	  
				<div class="form-group">
					<label for="address"> ವಿಳಾಸ: </label>
					<input refs="address" type="text" class="form-control" size="30" placeholder="ವಿಳಾಸ" onChange={this.handleChange.bind(this, "address")} value={this.state.fields["address"]}/>
					<span className="error">{this.state.errors["address"]}</span>
				</div>
				<div class="form-row">				
					<label for="gender" class="radio-label">ಲಿಂಗ:</label>
					<div class="form-radio-item">
						<input refs="gender" type="radio" name="gender" id="male" onChange={this.handleChange.bind(this, "gender")} value={this.state.fields["gender"]} checked/>
						<label for="male">ಗಂಡು</label>
						<span class="check"></span>
					</div>
					<div class="form-radio-item">
						<input refs="gender"  type="radio" name="gender" id="female" onChange={this.handleChange.bind(this, "gender")} value={this.state.fields["gender"]} />
						<label for="female">ಹೆಣ್ಣು</label>
						<span class="check"></span>
					</div>
				    <span className="error">{this.state.errors["gender"]}</span>
				
				</div>
				<div class="form-group">
					<label for="state">ತಾಲ್ಲೂಕು :</label>
					<div class="form-select">
						<select name="state" id="state" class="form-control" onChange={this.handleChange.bind(this, "taluk")} value={this.state.fields["taluk"]}>
							<option value=""></option>
							<option value="chitradurga">ಚಿತ್ರದುರ್ಗ</option>
							<option value="challakere">ಚಳ್ಳಕೆರೆ </option>
							<option value="hiriyur">ಹಿರಿಯೂರು  </option>
							<option value="hosdurga">ಹೊಸದುರ್ಗ </option>
							<option value="holalkere">ಹೊಳಲ್ಕೆರೆ  </option>
							<option value="molakalmuru">ಮೊಳಕಾಲ್ಮುರು  </option>
						</select>
						<span class="select-icon"><hi class="zmdi zmdi-chevron-down"></hi></span>
					</div>
					<span className="error">{this.state.errors["taluk"]}</span>
				</div>
				<div class="form-group">
						<label for="comp">ಸೌಲಭ್ಯದ ವಿವರ :</label>
						<input type="text" name="comp" id="comp"  class="form-control"onChange={this.handleChange.bind(this, "comp")} value={this.state.fields["comp"]} />
						<span className="error">{this.state.errors["comp"]}</span>					
				</div>
				 <div align="center">
				<button class="btn btn-primary btn-lg btn-block" id="submit" value="Submit">ನೋಂದಣೆ ಮಾಡಿ</button>
					</div>
			</form>
		</div>
    )
  }
}

export default (register);
