import {AgGridReact} from 'ag-grid-react';

import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import React, {Component} from "react";
import ReactDOM from 'react-dom';
import DatePicker from "react-datepicker"; 
import "react-datepicker/dist/react-datepicker.css";
import './App.css';
import logo from './logo.svg';

class edit extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			columnDefs: [
				{headerName: "ರೈತನ ಹೆಸರು", field: "name", checkboxSelection: true, editable: true},
				{headerName: "ವಿಳಾಸ", field: "address", editable: true},
				{headerName: "ತಾಲ್ಲೂಕು", field: "taluk", editable: true},
				{headerName: "ಲಿಂಗ:", field: "gender", editable: true},
				{headerName: "ವ್ಯವಸಾಯ  ಇಲಾಖೆ ಸೌಲಭ್ಯ", field: "agriculture", editable: true}

			],
			rowData: [
				{name: "ಸಿದ್ದಪ್ಪ", address: "ಮುದುವಾಡಿ,ಹೊಳಲ್ಕೆರೆ", taluk: "ಹೊಳಲ್ಕೆರೆ", gender:"ಗಂಡು", agriculture:"50000"},
				{name: "ಶಿವರಾಮು", address: "ಮುದುವಾಡಿ,ಹೊಳಲ್ಕೆರೆ", taluk: "ಹೊಳಲ್ಕೆರೆ", gender:"ಗಂಡು", agriculture:"50000"},
				{name: "ಪೊನ್ನಪ್ಪ", address: "ಮುದುವಾಡಿ,ಹೊಳಲ್ಕೆರೆ", taluk: "ಹೊಳಲ್ಕೆರೆ", gender:"ಗಂಡು", agriculture:"50000"}
			],
			startDate: new Date(),
			endDate: new Date()
		};
		
		
		this.handleChange = this.handleChange.bind(this);
	}
	
	/*componentDidMount() {
       fetch('https://api.myjson.com/bins/15psn9')
            .then(result => result.json())
            .then(rowData => this.setState({rowData}))
    }
	*/
	handleChange(date) {
		this.setState({
		  startDate: date,
		  endDate: date
		});
	}
	
	onAddRow() {
		var newItem = createNewRowData();
		var res = this.gridApi.updateRowData({ add: [newItem] });
	}
  
	onRemoveSelected() {
		var selectedData = this.gridApi.getSelectedRows();
		var res = this.gridApi.updateRowData({ remove: selectedData });
	}
	
	render() {
		return (
		<div class=".container-fluid">
			<br/>
			<div class="form-row1">
				<div class="form-group">
					<span>ದಿನಾಂಕದಿಂದ: <DatePicker selected={this.state.startDate}onChange={this.handleChange}/></span>
				</div>
				&nbsp;&nbsp;&nbsp;
				<div class="form-group">
					<span>ಇಲ್ಲಿಯವರೆಗೆ: <DatePicker selected={this.state.endDate}onChange={this.handleChange}/></span>
				</div>
			</div>		
			<div
				className="ag-theme-balham"
				style={{
					height: '500px',
					width: '1400px'
				}}
			>
				<AgGridReact
					onGridReady={ params => this.gridApi = params.api }
					enableSorting={true}
					animateRows={true}
					enableFilter={true}
					pagination={true}
					columnDefs={this.state.columnDefs}
					rowSelection="multiple"
					rowData={this.state.rowData}>
				</AgGridReact>
				<div class="row">
					<div class="text-center col-md-12 box">
						<button class="btn btn-primary btn-lg " id="submit" onClick={this.onButtonClick}>ಸಲ್ಲಿಸಿ</button>&nbsp;&nbsp;&nbsp;
						<button class="btn btn-primary btn-lg " id="delete" onClick={this.onRemoveSelected.bind(this)}>ಆಯ್ಕೆ ಅಳಿಸಿ</button>
					</div>					
				</div>
			</div>
		</div>
		);
	}
	
	onButtonClick = e => {
		const selectedNodes = this.gridApi.getSelectedNodes()  
		const selectedData = selectedNodes.map( node => node.data )
		const selectedDataStringPresentation = selectedData.map( node => node.name + ' ' + node.address).join(', ')
		alert(`Selected nodes: ${selectedDataStringPresentation}`) 
		
		//Make a REST call here
	}
}

function createNewRowData() {
		var newData = {
			name: "",
			address: "",
			taluk: "",
			gender: "",
			agriculture: "",
			education: "",
			revenue: "",
			health:""
		};
		return newData;
}

export default (edit);