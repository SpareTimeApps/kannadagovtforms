# READ ME #

### Overview ###

This is a proposal product to solve a particular requirement for the local state govt in India in maintaining the farming demographic information. 
The district authority needed to maintain data related to adverse effect of heavy rains and draughts and climatic condition on the farmers 
to understand how to provide relief and support. 

In some of the unfortunate regions in India, farmers suffer the consequences of climate changes aned drought and ended their lives. 
The local goverment wants to involve and support the farmers in these conditions. 

### Functional Details ###
The application is mainly built to maintain property details, family and debt information and suicide information of farmers each year 
So that the local authority can put forward a business case to the higher authorities for fund sanctions. 


### Technical details ###

The application is built using NodeJS and ReactJS.  It makes use of WebPack, react router, Bootstrap, SCSS,  Axios, react PDF js infinite, 
and relevant libraries that are typically used to build a NodeJS-ReactJS app. 

It was only a demo application to explain how the functionality can be achieved. 
