var path = require('path');
var webpack = require('webpack');

module.exports = {
  performance: {
     hints: process.env.NODE_ENV === 'production' ? "warning" : false
  },
    devServer: {
        inline: true,
        contentBase: './public',
        historyApiFallback: true,
        host: '0.0.0.0',
		port: process.env.PORT || 3000,
		compress: true,
		disableHostCheck: true, 
    },
    devtool: 'cheap-module-eval-source-map',
    entry: './src/index.js',
    module: {
        
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                    loader:"babel-loader",
					options: {
						presets: ['@babel/preset-env',
								  '@babel/react',{
								  'plugins': ['@babel/plugin-proposal-class-properties']}]
					}            
            },
            {
                   test: /\.scss/,
                    loader: 'style-loader!css-loader!sass-loader'
                
            }, 
            {
                test: /\.css$/,
                use: [ 'style-loader', 'css-loader' ]
              }          ,   
            {
                test: /\.png$/,
                loader: "url-loader?limit=100000"
            },
            {
                test: /\.jpg$/,
                loader: "file-loader"
            },{
                test: /\.jpeg$/,
                loader: "file-loader"
            },
            {
                test: /\.gif$/,
                loader: "file-loader"
            },
            {
                test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url-loader?limit=10000&mimetype=application/font-woff'
            },
            {
                test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url-loader?limit=10000&mimetype=application/octet-stream'
            },
            {
                test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'file-loader'
            },
            {
                test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url-loader?limit=10000&mimetype=image/svg+xml'
            }, {
                test: /\.json$/,
                loader: 'json-loader'
            },{
                test: /\.pdf$/,
                loader: "file-loader"
            }
        ]
    },
    output: {
        path: path.resolve('./public'),
        filename: 'js/bundle.min.js'
    },
    plugins: [
        new webpack.optimize.OccurrenceOrderPlugin()
    ],
    node: {
        net: 'empty',
        tls: 'empty',
        dns: 'empty',
        fs: 'empty',
        child_process: 'empty'
    }
};
